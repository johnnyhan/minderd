<script>
    module.exports = {
        data() {
            return {
                list: [],
                list1: ['a', 'b', 'c', 'd', 'e']
            };
        },

        created() {
            setTimeout(() => {
                this.list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'];
            }, 5000);
        }
    };
</script>

<style lang="scss" scoped>
.list-wrap {
    font-size: 16px;
    height: 200px;


    .item {
        list-style: none;
        margin: 0;
        padding: 0;
        height: 30px;
        line-height: 30px;
    }
}
</style>

## Scroll滚动

无缝滚动

### 基础用法

默认配置

:::demo 要使用Scroll组件，需要给Scroll设置一个高度

```html
<template>
    <el-scroll :list="list" :duration="100" class="list-wrap">
        <li v-for="item in list" :key="item" class="item">
            {{ item.repeat(20) }}
        </li>
    </el-scroll>
</template>

<script>
  export default {
      data() {
          return {
              list: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
          };
      },
  }
</script>

<style lang="scss" scoped>
.list-wrap {
    font-size: 16px;
    height: 200px;

    .item {
        list-style: none;
        margin: 0;
        padding: 0;
        height: 30px;
        line-height: 30px;
    }
}
</style>
```
:::

### 数据量不够

:::demo

```html
<template>
    <el-scroll :list="list" :duration="100" class="list-wrap">
        <li v-for="item in list1" :key="item" class="item">
            {{ item.repeat(20) }}
        </li>
    </el-scroll>
</template>

<script>
  export default {
      data() {
          return {
              list1: ['a', 'b', 'c']
          };
      },
  }
</script>

<style lang="scss" scoped>
.list-wrap {
    font-size: 16px;
    height: 200px;

    .item {
        list-style: none;
        margin: 0;
        padding: 0;
        height: 30px;
        line-height: 30px;
    }
}
</style>
```
:::