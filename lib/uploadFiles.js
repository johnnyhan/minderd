module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 292);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 106:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    name: 'ElUploadFiles',

    data: function data() {
        return {
            uploadFileList: [],
            dialogVisible: false,
            mapping: [],
            dialogImageUrl: '',
            repeatRemove: false,
            mp4: false
        };
    },


    props: {
        size: String,
        accept: {
            type: String,
            default: ''
        },
        limit: {
            type: Number,
            default: Number.MAX_VALUE
        },
        disabled: {
            type: Boolean,
            default: false
        },
        fileList: {
            type: Array,
            default: function _default() {
                return [];
            }
        },
        action: {
            required: true,
            type: String
        },
        tip: {
            type: String,
            default: ''
        },
        headers: {
            type: Object,
            default: function _default() {
                return {};
            }
        },
        value: {
            type: Array,
            default: function _default() {
                return [];
            }
        },
        trigger: {
            type: String,
            default: ''
        },
        isBackground: {
            type: Boolean,
            default: false
        },
        visiblePreview: {
            type: Boolean,
            default: true
        },
        params: {
            type: Object,
            default: function _default() {
                return {};
            }
        },
        drag: Boolean,
        name: String,
        withCredentials: Boolean,
        onExceed: Function,
        className: String
    },
    computed: {
        requestHeader: function requestHeader() {
            var header = {
                'X-Requested-With': 'XMLHttpRequest',
                'Token': '',
                'Language': '',
                'Accept-Language': 'en-us;q=0.8',
                'Accept': 'application/json, text/plain, */*'
            };
            if (this.headers) {
                return Object.assign({}, header, this.headers);
            } else {
                return header;
            }
        }
    },
    methods: {
        handlePictureCardPreview: function handlePictureCardPreview(file) {
            var index = file.name.lastIndexOf('.');
            var ext = file.name.substr(index + 1);
            if (ext.indexOf('丨') !== -1) {
                ext = ext.split('丨')[0];
            }
            ext = ext.toLowerCase();
            if (ext === 'mp4') {
                this.dialogVisible = true;
                this.dialogImageUrl = file.url;
                this.mp4 = true;
                return;
            }
            if (ext === 'png' || ext === 'jpg' || ext === 'jpeg' || ext === 'gif') {
                this.dialogVisible = true;
                this.mp4 = false;
                if (file.url.indexOf('!') !== -1) {
                    this.dialogImageUrl = file.url.split('!')[0];
                } else {
                    this.dialogImageUrl = file.url;
                }
                return;
            }
            location.href = file.url;
        },
        remove: function remove(file, fileList) {
            this.getValueList(fileList);
            if (!this.repeatRemove) {
                if (file.raw) {
                    var hash = file.raw.__hash;
                    this.mapping.splice(this.mapping.indexOf(hash), 1);
                }
            }
            this.repeatRemove = false;
        },
        success: function success(response, file, fileList) {
            this.getValueList(fileList);
        },
        getValueList: function getValueList(fileList) {
            this.value.length = 0;
            for (var _iterator = fileList, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
                var _ref;

                if (_isArray) {
                    if (_i >= _iterator.length) break;
                    _ref = _iterator[_i++];
                } else {
                    _i = _iterator.next();
                    if (_i.done) break;
                    _ref = _i.value;
                }

                var i = _ref;

                this.value.push(i);
            }
            this.$emit('input', this.value);
        },
        beforeUpload: function beforeUpload(file) {
            var hash = file.__hash || (file.__hash = this.hashString(file.name + file.size + file.lastModified));

            if (this.mapping.indexOf(hash) !== -1) {
                this.$emit('error', {
                    repeat: true,
                    message: '重复文件'
                });
                this.repeatRemove = true;
                return false;
            } else {
                this.mapping.push(hash);
            }
        },
        hashString: function hashString(str) {
            var hash = 0;
            var i = 0;
            var len = str.length;
            var _char = void 0;

            for (; i < len; i++) {
                _char = str.charCodeAt(i);
                hash = _char + (hash << 6) + (hash << 16) - hash;
            }

            return hash;
        }
    },

    watch: {
        value: {
            deep: true,
            immediate: true,
            handler: function handler(n, o) {
                this.uploadFileList = n;
            }
        }
    }
};

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _Main = __webpack_require__(293);

var _Main2 = _interopRequireDefault(_Main);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_Main2.default.install = function (Vue) {
    Vue.component(_Main2.default.name, _Main2.default);
};

exports.default = _Main2.default;

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_ab5a9e6a_hasScoped_false_preserveWhitespace_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Main_vue__ = __webpack_require__(294);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_ab5a9e6a_hasScoped_false_preserveWhitespace_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Main_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('el-upload',{attrs:{"is-background":_vm.isBackground,"file-list":_vm.fileList,"multiple":"","limit":_vm.limit,"size":_vm.size,"action":_vm.action,"accept":_vm.accept,"data":_vm.params,"headers":_vm.requestHeader,"list-type":"picture-card","before-upload":_vm.beforeUpload,"on-remove":_vm.remove,"on-success":_vm.success,"drag":_vm.drag,"name":_vm.name,"disabled":_vm.disabled,"with-credentials":_vm.withCredentials,"on-exceed":_vm.onExceed,"on-preview":_vm.handlePictureCardPreview}},[_c('el-icon',{attrs:{"name":"increase"}}),_c('div',{attrs:{"slot":"tip"},slot:"tip"},[_vm._t("tip",[_vm._v(_vm._s(_vm.tip))])],2)],1),(_vm.visiblePreview)?_c('el-dialog',{staticClass:"preview-image",attrs:{"visible":_vm.dialogVisible},on:{"update:visible":function($event){_vm.dialogVisible=$event}}},[(!_vm.mp4)?_c('img',{attrs:{"src":_vm.dialogImageUrl,"alt":""}}):_c('video',{attrs:{"src":_vm.dialogImageUrl,"controls":"controls"}})]):_vm._e()],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ })

/******/ });