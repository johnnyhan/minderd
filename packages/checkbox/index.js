import ElCheckbox from './src/Main';

/* istanbul ignore next */
ElCheckbox.install = function (Vue) {
	Vue.component(ElCheckbox.name, ElCheckbox);
};

export default ElCheckbox;
