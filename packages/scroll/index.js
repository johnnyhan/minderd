import Scroll from './src/scroll';

/* istanbul ignore next */
Scroll.install = function(Vue) {
	Vue.component(Scroll.name, Scroll);
};

export default Scroll;
