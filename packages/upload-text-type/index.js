import UploadTextType from './src/Main';

UploadTextType.install = function (Vue) {
    Vue.component(UploadTextType.name, UploadTextType);
};

export default UploadTextType;
