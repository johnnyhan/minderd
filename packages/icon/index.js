import Icon from './src/Main';

Icon.install = function(Vue) {
    Vue.component(Icon.name, Icon);
}

export default Icon;
