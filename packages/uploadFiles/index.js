import UploadFiles from './src/Main';

UploadFiles.install = function (Vue) {
    Vue.component(UploadFiles.name, UploadFiles);
};

export default UploadFiles;
