import Currency from './src/Main';

Currency.install = function(Vue) {
	Vue.component(Currency.name, Currency);
};

export default Currency;
